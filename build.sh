#!/bin/bash

find . -type f -name '*.bu' -exec bash -c 'docker run -i --rm quay.io/coreos/butane:release --pretty --strict < "{}" > $(echo "{}" | sed "s/bu$/ign/")' \;
